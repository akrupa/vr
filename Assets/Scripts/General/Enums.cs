﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

enum PhotonEventCodes : byte
{
    GameStarted,
    FirstTeamScoredGoal,
    SecondTeamScoredGoal,
    GameEnded,
}
