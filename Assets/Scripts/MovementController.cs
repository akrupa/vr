﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour
{

    private Rigidbody playerRigidbody;
    private const float forceConstant = 2.0f; 
    private float soundEffectVolume = 1.0f;

    public Transform ballCaughtPosition;
	private bool _interactionEnabled = true;
    public bool interactionEnabled
    {
        get { return _interactionEnabled; }
        set { _interactionEnabled = value; GetComponent<CrosshairVisuals>().enabled = _interactionEnabled; }
    }

    private const float sensitivityX = 1.5f;
    private const float sensitivityY = 1.5f;

    private const float DampingConstant = 0.99f;
    private const float VelocityEpsSqr = 0.001f;

    private const float PushBallCooldownInSeconds = 1.0f;
    private float LastPushBallTime;

    public AudioSource MatchMusicAudioSource;
    public AudioSource EngineAudioSource;
    public AudioSource ScoredGoalAudioSource;
    public AudioSource LostGoalAudioSource;

    private float InitialMatchMusicVolume;

    public void Start()
    {
        InitialMatchMusicVolume = MatchMusicAudioSource.volume;

        LastPushBallTime = -PushBallCooldownInSeconds;

        playerRigidbody = transform.parent.parent.GetComponent<Rigidbody>();
        if (playerRigidbody == null)
        {
            Debug.LogError("playerRigidbody == null");
        }

        AudioManager.Instance.PlayMusic(MatchMusicAudioSource);

        EngineAudioSource.loop = true;
        EngineAudioSource.volume = soundEffectVolume;
        EngineAudioSource.mute = true;
        AudioManager.Instance.PlaySound(EngineAudioSource);
    }

    private void PullBallWithGun()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position, transform.forward, out hitInfo) && hitInfo.collider.gameObject == MatchManager.Instance.BallRootGameObject)
        {
            MatchManager.Instance.PlayerPhotonView.RPC("PullBallWithGunRPC", PhotonTargets.MasterClient, PhotonNetwork.player.ID);
        }
    }

    private void PushBallWithGun()
    {
        RaycastHit hitInfo;
        if (Time.time - LastPushBallTime >= PushBallCooldownInSeconds && Physics.Raycast(transform.position, transform.forward, out hitInfo) && hitInfo.collider.gameObject == MatchManager.Instance.BallRootGameObject)
        {
            MatchManager.Instance.PlayerPhotonView.RPC("PushBallWithGunRPC", PhotonTargets.MasterClient, PhotonNetwork.player.ID);
            LastPushBallTime = Time.time;
        }
    }

    public void Update()
    {
        bool isEngineActive = false;

		if (interactionEnabled)
		{

				if (Input.GetKey (KeyCode.W)) {
						playerRigidbody.AddForce (transform.forward * forceConstant);
						isEngineActive = true;
				}
				if (Input.GetKey (KeyCode.S)) {
						playerRigidbody.AddForce (-transform.forward * forceConstant);
						isEngineActive = true;
				}
				if (Input.GetKey (KeyCode.D)) {
						playerRigidbody.AddForce (transform.right * forceConstant);
						isEngineActive = true;
				}
				if (Input.GetKey (KeyCode.A)) {
						playerRigidbody.AddForce (-transform.right * forceConstant);
						isEngineActive = true;
				}
				if (Input.GetKey (KeyCode.E)) {
						playerRigidbody.AddForce (transform.up * forceConstant);
						isEngineActive = true;
				}
				if (Input.GetKey (KeyCode.Q)) {
						playerRigidbody.AddForce (-transform.up * forceConstant);
						isEngineActive = true;
				}


				float mouseInputX = Input.GetAxis ("Mouse X") * sensitivityX;
				if (mouseInputX != 0.0f) {
						playerRigidbody.AddTorque (transform.up * mouseInputX);
				}
				float mouseInputY = Input.GetAxis ("Mouse Y") * sensitivityY;
				if (mouseInputY != 0.0f) {
						playerRigidbody.AddTorque (-transform.right * mouseInputY);
				}

				if (Input.GetMouseButton (0)) {
						PullBallWithGun ();
				}
				if (Input.GetMouseButton (1)) {
						PushBallWithGun ();
				}
		}

        //Damping
        playerRigidbody.velocity *= DampingConstant;
        if (playerRigidbody.velocity.sqrMagnitude < VelocityEpsSqr)
        {
            playerRigidbody.velocity = Vector3.zero;
        }

        playerRigidbody.angularVelocity *= DampingConstant;
        if (playerRigidbody.angularVelocity.sqrMagnitude < VelocityEpsSqr)
        {
            playerRigidbody.angularVelocity = Vector3.zero;
        }

        UpdateAudio(isEngineActive);
    }

    private void UpdateAudio(bool isEngineActive)
    {
        MatchMusicAudioSource.volume = ScoredGoalAudioSource.isPlaying || LostGoalAudioSource.isPlaying ? 0 : InitialMatchMusicVolume;

        if (isEngineActive)
        {
            if (EngineAudioSource.mute)
            {
                EngineAudioSource.mute = false;
            }
            if (EngineAudioSource.volume < soundEffectVolume)
            {
                EngineAudioSource.volume += 5f * Time.deltaTime;
            }
        }
        else
        {
            if (EngineAudioSource.volume > 0.01f)
            {
                EngineAudioSource.volume -= 5f * Time.deltaTime;
            }
            else
            {
                EngineAudioSource.mute = true;
            }
        }
    }

}
