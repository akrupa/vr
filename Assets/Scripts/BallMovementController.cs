﻿using UnityEngine;
using System.Collections;

public class BallMovementController : MonoBehaviour
{
    private const float DampingConstant = 0.99f;
    private const float VelocityEpsSqr = 0.001f;

	void FixedUpdate()
    {
        rigidbody.velocity *= DampingConstant;
        if (rigidbody.velocity.sqrMagnitude < VelocityEpsSqr)
        {
            rigidbody.velocity = Vector3.zero;
        }

        rigidbody.angularVelocity *= DampingConstant;
        if (rigidbody.angularVelocity.sqrMagnitude < VelocityEpsSqr)
        {
            rigidbody.angularVelocity = Vector3.zero;
        }
	}
}
