﻿using UnityEngine;
using System.Collections;

public class GoalScript : MonoBehaviour
{
    public bool IsFirstTeamGoal;

    void OnCollisionEnter(Collision collision)
    {
        if (PhotonNetwork.isMasterClient && !MatchManager.Instance.IsResettingAfterScoreNow)
        {
            if (collision.gameObject == MatchManager.Instance.BallRootGameObject.gameObject)
            {
                MatchManager.Instance.IsResettingAfterScoreNow = true;
                if (IsFirstTeamGoal)
                {
                    PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.SecondTeamScoredGoal, null, true, new RaiseEventOptions() { Receivers = ExitGames.Client.Photon.Lite.ReceiverGroup.All });
                }
                else
                {
                    PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.FirstTeamScoredGoal, null, true, new RaiseEventOptions() { Receivers = ExitGames.Client.Photon.Lite.ReceiverGroup.All });
                }
            }
        }
    }
}
