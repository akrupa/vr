﻿using UnityEngine;
using System.Collections;

public class BallVisuals : MonoBehaviour
{
    private const float ScrollSpeed = 0.5F;
    void Update()
    {
        float offset = Time.time * ScrollSpeed;
        renderer.material.SetTextureOffset("_MainTex", new Vector2(0, offset));
    }
}
