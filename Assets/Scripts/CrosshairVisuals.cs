﻿using UnityEngine;
using System.Collections;

public class CrosshairVisuals : MonoBehaviour
{
    Rect crosshairRect;
    int lastScreenWidth, lastScreenHeight;

    public Texture CrosshairTexture;
    public float SizeMultiplier = 0.01f;

    void CalculateCrosshairRect()
    {
        float crosshairSize = Mathf.Min(Screen.width,Screen.height) * SizeMultiplier;
        crosshairRect = new Rect(Screen.width / 2 - crosshairSize / 2, Screen.height / 2 - crosshairSize / 2, crosshairSize, crosshairSize);
    }

    void Start()
    {
        CalculateCrosshairRect();
        lastScreenHeight = Screen.height;
        lastScreenWidth = Screen.width;
    }

    void Update()
    {
        if (lastScreenWidth != Screen.width || lastScreenHeight != Screen.height)
        {
            CalculateCrosshairRect();
            lastScreenHeight = Screen.height;
            lastScreenWidth = Screen.width;
        }
    }

    void OnGUI()
    {
        GUI.DrawTexture(crosshairRect, CrosshairTexture);
    }
}
