﻿using UnityEngine;
using System.Collections;
using System;

using System.Linq;

public class MultiplayerWaitingScreen : GUIScreen
{
    public dfListbox playersListBox;
    public dfButton StartGameButton;
	public int MapIndex;

    private void Start()
    {
        PhotonNetwork.OnEventCall += this.OnGameStartedEvent;
    }

    public void OnToggleReadyButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        var customProperties = new ExitGames.Client.Photon.Hashtable();
        foreach (var item in PhotonNetwork.player.customProperties)
        {
            customProperties.Add(item.Key, item.Value);
        }
        customProperties["IsReady"] = !(bool)PhotonNetwork.player.customProperties["IsReady"];
        PhotonNetwork.player.SetCustomProperties(customProperties);
        Refresh();
    }

    public void OnToggleTeamButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        var customProperties = new ExitGames.Client.Photon.Hashtable();
        foreach (var item in PhotonNetwork.player.customProperties)
        {
            customProperties.Add(item.Key, item.Value);
        }
        customProperties["IsInFirstTeam"] = !(bool)PhotonNetwork.player.customProperties["IsInFirstTeam"];
        PhotonNetwork.player.SetCustomProperties(customProperties);
        Refresh();
    }

    public void OnStartGameButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        Refresh();

        if (!PhotonNetwork.isMasterClient)
        {
            return;
        }

        bool areAllReady = PhotonNetwork.playerList.All(player => (bool)player.customProperties["IsReady"]);
        if (!areAllReady)
        {
            return;
        }

        var customProperties = new ExitGames.Client.Photon.Hashtable();
        foreach (var item in PhotonNetwork.room.customProperties)
        {
            customProperties.Add(item.Key, item.Value);
        }
        customProperties["IsStarted"] = true;
        PhotonNetwork.room.SetCustomProperties(customProperties);
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.GameStarted, MapIndex, true, new RaiseEventOptions() { Receivers = ExitGames.Client.Photon.Lite.ReceiverGroup.All });
    }

    public void OnBackButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        PhotonNetwork.LeaveRoom();

        //wait for OnJoinedLobby callback
    }

    void OnJoinedLobby()
    {
        if (!this.enabled)
        {
            return;
        }

        this.Hide();

        GUIManager.Instance.MultiplayerMainScreen.Show();
    }

    public void OnRefreshButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        Refresh();
    }

    public override void Show()
    {
        base.Show();

		StartCoroutine(RepeatedRefresh ());        
    }

	IEnumerator RepeatedRefresh()
	{
		Refresh ();
		yield return new WaitForSeconds(2.0f);
		if (this.gameObject.activeSelf) {
			StartCoroutine (RepeatedRefresh ());
		}
	}

    private void OnGameStartedEvent(byte eventCode, object content, int senderId)
    {
        if (eventCode != (byte)PhotonEventCodes.GameStarted)
        {
            return;
        }
		this.MapIndex = (int)content;

		GameObject[] maps = GUIManager.Instance.maps;
		for (int i = 0; i < maps.Count(); i++) 
		{
			maps[i].SetActive(i == this.MapIndex);
		}

        MatchManager.Instance.StartMatch(maps[this.MapIndex]);
        this.Hide();
        GUIManager.Instance.InMatchScreen.Show();
		Screen.showCursor = false;
		Screen.lockCursor = true;
    }

    private void Refresh()
    {
        playersListBox.SelectedIndex = -1;

        if (PhotonNetwork.inRoom)
        {
            StartGameButton.IsVisible = PhotonNetwork.isMasterClient;
            var playerInfos = PhotonNetwork.playerList;
            playersListBox.Items = playerInfos.Select(playerInfo => String.Format("[{2}] {0} {1}", playerInfo.name,
                (bool)playerInfo.customProperties["IsReady"] ? "Ready" : "-",
                (bool)playerInfo.customProperties["IsInFirstTeam"] ? "Alpha": "Beta"
                )).ToArray();
        }
        else
        {
            playersListBox.Items = new string[0];
            StartGameButton.IsVisible = false;
        }
    }
}
