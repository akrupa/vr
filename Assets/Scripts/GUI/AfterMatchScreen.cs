﻿using UnityEngine;
using System.Collections;
using System;

public class AfterMatchScreen : GUIScreen
{
    public dfLabel InfoLabel;

    public void Initialize(string firstTeamName, string secondTeamName, string matchScore)
    {
        InfoLabel.Text = String.Format("{0} {2} {1}", firstTeamName, secondTeamName, matchScore);
    }

    public void OnBackButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        PhotonNetwork.LeaveRoom();

        //wait for OnJoinedLobby callback
    }

    void OnJoinedLobby()
    {
        if (!this.enabled)
        {
            return;
        }

        this.Hide();
        GUIManager.Instance.MainMenuScreen.Show();
    }
}
