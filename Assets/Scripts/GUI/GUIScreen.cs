﻿using UnityEngine;
using System.Collections;

public abstract class GUIScreen : MonoBehaviour
{
    public virtual void Show()
    {
        gameObject.SetActive(true);
        var screen = gameObject.GetComponent<dfPanel>();
        screen.Width = Screen.width;
        screen.Height = Screen.height;
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}
