﻿using UnityEngine;
using System.Collections;

public class OptionsScreen : GUIScreen
{
    public dfTextbox NicknameTextbox;
    public dfCheckbox MusicOnCheckbox;
    public dfCheckbox SoundOnCheckbox;



    public void OnSaveChangesClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        OptionsManager.Instance.PlayerNickname = NicknameTextbox.Text;
        OptionsManager.Instance.IsSoundOn = SoundOnCheckbox.IsChecked;
		OptionsManager.Instance.IsMusicOn = MusicOnCheckbox.IsChecked;

		ApplyChanges ();
    }

    public void OnBackClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        this.Hide();
        GUIManager.Instance.MainMenuScreen.Show();
    }

    public override void Show()
    {
        base.Show();

        NicknameTextbox.Text = OptionsManager.Instance.PlayerNickname;
        MusicOnCheckbox.IsChecked = OptionsManager.Instance.IsMusicOn;
        SoundOnCheckbox.IsChecked = OptionsManager.Instance.IsSoundOn;
    }

	private void ApplyChanges() {
		OptionsManager.Instance.Save ();
	}
}
