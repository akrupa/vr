﻿using UnityEngine;
using System.Collections;
using System;

using System.Linq;

public class MultiplayerCreateGameScreen : GUIScreen
{
    public dfTextbox GameNameTextBox;
    public dfTextbox MaxPlayersCountTextBox;
    public dfTextbox GameTimeTextBox;
	public dfListbox mapsListBox;

    public void OnCreateButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        string gameName = GameNameTextBox.Text;
        if (string.IsNullOrEmpty(gameName))
        {
            return;
        }

        int maxPlayersCount;
        if (!int.TryParse(MaxPlayersCountTextBox.Text, out maxPlayersCount) || maxPlayersCount<=0)
        {
            return;
        }

        TimeSpan gameTime;
        if (!TimeSpan.TryParse(GameTimeTextBox.Text, out gameTime) || gameTime.TotalMilliseconds <= 0.0)
        {
            return;
        }

        PhotonNetwork.player.name = OptionsManager.Instance.PlayerNickname;
        var customProperties = new ExitGames.Client.Photon.Hashtable();
        customProperties["IsReady"] = false;
        customProperties["IsInFirstTeam"] = true;
        PhotonNetwork.player.SetCustomProperties(customProperties);

        var customRoomProperties2 = new ExitGames.Client.Photon.Hashtable();
        customRoomProperties2["GameTimeSeconds"] = (double)gameTime.TotalSeconds;
        customRoomProperties2["IsStarted"] = false;
		customRoomProperties2["mapIndex"] = mapsListBox.SelectedIndex;
		customRoomProperties2 ["mapName"] = mapsListBox.SelectedValue;
        bool result = PhotonNetwork.CreateRoom(gameName, new RoomOptions() { maxPlayers = maxPlayersCount, isOpen = true, isVisible = true,
			customRoomProperties = customRoomProperties2, customRoomPropertiesForLobby = new string[] {"GameTimeSeconds", "IsStarted", "mapIndex", "mapName"} }, null);
        if (!result)
        {
            return;
        }
        //wait for OnJoinedRoom callback
    }

    void OnJoinedRoom()
    {
        if (!this.enabled)
        {
            return;
        }

        this.Hide();
		GUIManager.Instance.MultiplayerWaitingScreen.MapIndex = this.mapsListBox.SelectedIndex;
        GUIManager.Instance.MultiplayerWaitingScreen.Show();
    }

    public void OnBackButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        this.Hide();
        GUIManager.Instance.MultiplayerMainScreen.Show();
    }

    public override void Show()
    {
        base.Show();

		string[] items = new string [3];
		items[0] = "Space Station";
		items[1] = "Torus";
		items[2] = "Maze";
		mapsListBox.Items = items;
		mapsListBox.SelectedIndex = 0;

        GameNameTextBox.Text = "NoName";
        MaxPlayersCountTextBox.Text = 2.ToString();
        GameTimeTextBox.Text = "00:10:00";
    }
}
