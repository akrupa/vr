﻿using UnityEngine;
using System.Collections;
using System;

using System.Linq;

public class MultiplayerMainScreen : GUIScreen
{
    public dfListbox gamesListBox;

    public void OnCreateGameButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        this.Hide();
        GUIManager.Instance.MultiplayerCreateGameScreen.Show();
    }

    public void OnJoinGameButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        string selectedGameInfo = gamesListBox.SelectedItem;
        if (selectedGameInfo == null)
        {
            return; //no item selected
        }

        int lastApostrophePosition = selectedGameInfo.LastIndexOf('\'');
        int firstApostrophePosition = selectedGameInfo.IndexOf('\'');
        string selectedGameName = selectedGameInfo.Substring(firstApostrophePosition + 1, lastApostrophePosition-firstApostrophePosition-1);

        var selectedRoomInfo = PhotonNetwork.GetRoomList().First(roomInfo => roomInfo.name == selectedGameName);
        if ((bool)selectedRoomInfo.customProperties["IsStarted"])
        {
            return;
        }

        PhotonNetwork.player.name = OptionsManager.Instance.PlayerNickname;
        var customProperties = new ExitGames.Client.Photon.Hashtable();
        customProperties["IsReady"] = false;
        customProperties["IsInFirstTeam"] = true;
        PhotonNetwork.player.SetCustomProperties(customProperties);

        bool result = PhotonNetwork.JoinRoom(selectedGameName);
        if (!result)
        {
            return;
        }
        //wait for OnJoinedRoom callback
    }

    void OnJoinedRoom()
    {
        if (!this.enabled)
        {
            return;
        }

        this.Hide();
        GUIManager.Instance.MultiplayerWaitingScreen.Show();
    }

    public void OnBackButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        this.Hide();
        GUIManager.Instance.MainMenuScreen.Show();
    }

    public void OnRefreshButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        Refresh();
    }

    public override void Show()
    {
        base.Show();

		StartCoroutine( RepeatedRefresh ());
    }

	IEnumerator RepeatedRefresh()
    {
		Refresh();
        yield return new WaitForSeconds(2.0f);
		if (this.gameObject.activeSelf)
		{
			StartCoroutine (RepeatedRefresh ());
		}
    }

    private void Refresh()
    {
        gamesListBox.SelectedIndex = -1;

        if (PhotonNetwork.insideLobby)
        {
            var roomInfos = PhotonNetwork.GetRoomList();
            roomInfos = roomInfos.Where(roomInfo => !(bool)roomInfo.customProperties["IsStarted"]).ToArray();

			gamesListBox.Items = roomInfos.Select(roomInfo => String.Format("'{0}' {1}/{2} {3} {4}", roomInfo.name, roomInfo.playerCount, roomInfo.maxPlayers,
                (TimeSpan.FromSeconds((double)roomInfo.customProperties["GameTimeSeconds"])).ToString(), roomInfo.customProperties["mapName"].ToString())).ToArray();
        }
        else
        {
            gamesListBox.Items = new string[0];
        }
    }
}
