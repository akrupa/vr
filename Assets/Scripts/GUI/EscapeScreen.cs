﻿using UnityEngine;
using System.Collections;

public class EscapeScreen : GUIScreen 
{

	public override void Show ()
	{
		base.Show();
		var movementController = MatchManager.Instance.PlayerRootGameObject.transform.FindChild ("MainPlayer").FindChild ("Main Camera").GetComponent<MovementController> ();
		movementController.interactionEnabled = false;

	}

	public override void Hide()
	{
		base.Hide ();

        if (!MatchManager.Instance.IsMatchOver)
        {
            var movementController = MatchManager.Instance.PlayerRootGameObject.transform.FindChild("MainPlayer").FindChild("Main Camera").GetComponent<MovementController>();
            movementController.interactionEnabled = true;
        }
	}


	public void OnResumeButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
	{
		this.Hide();
		Screen.showCursor = false;
		Screen.lockCursor = true;
	}

	public void OnExitButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
	{
        ApplicationManager.Instance.CloseApplication();
	}

	public void OnMainMenuButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
	{
		PhotonNetwork.LeaveRoom();

        //wait for OnJoinedLobby callback
	}

    void OnJoinedLobby()
    {
        if (!this.enabled)
        {
            return;
        }

        MatchManager.Instance.EndMatch();

        //this.Hide() is invoked now by GUIManager.Instance.InMatchScreen.Hide(); instruction below
        GUIManager.Instance.InMatchScreen.Hide();
        GUIManager.Instance.MainMenuScreen.Show();
    }
}
