﻿using UnityEngine;
using System.Collections;

public class MainMenuScreen : GUIScreen
{
    public void OnMultiplayerButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        if (PhotonNetwork.insideLobby)
        {
            SwitchToMultiplayerMainWindow();
        }
        else
        {
            bool result = PhotonNetwork.ConnectUsingSettings("0.1");
            //wait for OnJoinedLobby callback
        }
    }

    void OnJoinedLobby()
    {
        if (!this.enabled)
        {
            return;
        }

        SwitchToMultiplayerMainWindow();
    }

    private void SwitchToMultiplayerMainWindow()
    {
        this.Hide();
        GUIManager.Instance.MultiplayerMainScreen.Show();
    }

    public void OnOptionsButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        this.Hide();
        GUIManager.Instance.OptionsScreen.Show();
    }

    public void OnExitButtonClick(dfControl control, dfMouseEventArgs mouseEvent)
    {
        ApplicationManager.Instance.CloseApplication();
    }
}
