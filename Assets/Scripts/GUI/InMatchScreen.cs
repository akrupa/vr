﻿using UnityEngine;
using System.Collections;

public class InMatchScreen : GUIScreen
{
    public dfLabel FirstTeamScoreLabel;
    public dfLabel SecondTeamScoreLabel;
    public dfLabel TimeLeftLabel;
	public dfLabel PlayerLeftLabel;

    private bool escapeWasPressed = false;

    private void Update()
    {
        bool escapeIsPressed = Input.GetKeyDown(KeyCode.Escape);

        if (!escapeWasPressed && escapeIsPressed)
        {
            if (GUIManager.Instance.EscapeScreen.gameObject.activeSelf)
            {
                GUIManager.Instance.EscapeScreen.Hide();
				Screen.showCursor = false;
				Screen.lockCursor = true;
			}
            else
            {
                GUIManager.Instance.EscapeScreen.Show();
				Screen.showCursor = true;
				Screen.lockCursor = false;
			}
        }

        escapeWasPressed = escapeIsPressed;
    }

    public override void Hide()
    {
        base.Hide();
        if (GUIManager.Instance.EscapeScreen.gameObject.activeSelf)
        {
            GUIManager.Instance.EscapeScreen.Hide();
        }
    }
}
