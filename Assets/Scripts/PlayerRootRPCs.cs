﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class PlayerRootRPCs : MonoBehaviour
{
    private const float PullForceConstant = 5.0f;
    private const float PullEpsilon = 1.0f;
    private const float PushForceConstant = 500.0f;

    [RPC]
    private void PullBallWithGunRPC(int playerID)
    {

       GameObject pullingPlayerObj = MatchManager.Instance.FindPlayerRootObjById(playerID);
       if (pullingPlayerObj == null)
       {
            return;
       }

       Vector3 ballCaughtPosition = pullingPlayerObj.transform.position +pullingPlayerObj.transform.right*3;
       float distanceFromCaughtPosition = (MatchManager.Instance.BallRootGameObject.transform.position - ballCaughtPosition).magnitude;
       if (distanceFromCaughtPosition < PullEpsilon)
       {
           MatchManager.Instance.BallRootGameObject.rigidbody.velocity = Vector3.zero;
           MatchManager.Instance.BallRootGameObject.rigidbody.angularVelocity = Vector3.zero;
           MatchManager.Instance.BallRootGameObject.transform.position = ballCaughtPosition;
       }
       else
       {
           Vector3 forceVersor = (ballCaughtPosition - MatchManager.Instance.BallRootGameObject.transform.position).normalized;
           MatchManager.Instance.BallRootGameObject.rigidbody.AddForce((PullForceConstant) * forceVersor);
       }
    }

    [RPC]
    private void PushBallWithGunRPC(int playerID)
    {
        GameObject pushingPlayerObj = MatchManager.Instance.FindPlayerRootObjById(playerID);
        if (pushingPlayerObj == null)
        {
            return;
        }

        MatchManager.Instance.BallRootGameObject.rigidbody.AddForce(PushForceConstant * (MatchManager.Instance.BallRootGameObject.transform.position - pushingPlayerObj.transform.position).normalized);
    }
}
