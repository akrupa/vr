﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class MatchManager : Singleton<MatchManager>
{
    protected MatchManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public bool IsResettingAfterScoreNow;

	public SpawnPoints SpawnPoints
	{
		get
		{
			return (SpawnPoints)this.Map.GetComponent("SpawnPoints");
		}
	}

    public Transform BallRespawnTransform 
	{
		get
		{
			return this.SpawnPoints.BallSpawnPointTransform;
		}
	}
    public Transform FirstTeamRespawnTransform
	{
		get
		{
			return this.SpawnPoints.FirstTeamSpawnPointTransform;
		}
	}
    public Transform SecondTeamRespawnTransform
	{
		get
		{
			return this.SpawnPoints.SecondTeamSpawnPointTransform;
		}
	}
	
    public PhotonView PlayerPhotonView;
    public GameObject PlayerRootGameObject;

	public GameObject Map;

    private GameObject _ballRootGameObject;
    public GameObject BallRootGameObject
    {
        get
        {
            if (_ballRootGameObject == null)
            {
                _ballRootGameObject = GameObject.Find("Ball(Clone)");
            }
            return _ballRootGameObject;
        }
    }

    private int FirstTeamScore;
    private int SecondTeamScore;

    private double MatchTimeInSeconds;
    private float BeginTime;
    public bool IsMatchOver;

    private PhotonView[] PlayersPhotonViews
    {
        get
        {
            return UnityEngine.Object.FindObjectsOfType<PhotonView>().Where(view => !view.gameObject.name.StartsWith("Ball")).ToArray();
        }
    }

    private void Start()
    {
        PhotonNetwork.OnEventCall += OnFirstTeamScoredGoalEvent;
        PhotonNetwork.OnEventCall += OnSecondTeamScoredGoalEvent;
        PhotonNetwork.OnEventCall += OnGameEndedEvent;
    }

    public void StartMatch(GameObject mapObject)
    {
		Map = mapObject;

		IsMatchOver = false;
        IsResettingAfterScoreNow = false;

        PlayerRootGameObject = PhotonNetwork.Instantiate("PlayerRoot", Vector3.zero, Quaternion.identity, 0);
        PlayerRootGameObject.transform.FindChild("MainPlayer").gameObject.SetActive(true);
        PlayerRootGameObject.transform.FindChild("OtherPlayer").gameObject.SetActive(false);
		 GameObject.Find ("SpaceStation");

        PlayerPhotonView = PlayerRootGameObject.GetComponent<PhotonView>();

        if (PhotonNetwork.isMasterClient)
        {
            _ballRootGameObject = PhotonNetwork.InstantiateSceneObject("Ball", Vector3.zero, Quaternion.identity, 0, null);
        }

        var playerCamera = PlayerRootGameObject.transform.FindChild("MainPlayer").FindChild("Main Camera").camera;
        CameraManager.Instance.ChangeCamera(playerCamera);
        foreach (var renderer in Map.transform.GetComponentsInChildren<Renderer>())
        {
            renderer.enabled = true;
        }

        FirstTeamScore = 0;
        SecondTeamScore = 0;
        RefreshScoreLabels();

        BeginTime = Time.time;
        MatchTimeInSeconds = (double)PhotonNetwork.room.customProperties["GameTimeSeconds"];
        IsMatchOver = false;

        ResetMatchAfterScoredGoal();
    }

	public void EndMatch()
	{
        //Debug.LogWarning("isMaster = " + PhotonNetwork.isMasterClient);
        //if (PhotonNetwork.isMasterClient)
        //{
			PhotonNetwork.DestroyAll();
			PhotonNetwork.Destroy(BallRootGameObject);
        //}
		PlayerRootGameObject = null;
		_ballRootGameObject = null;
		PlayerPhotonView = null;
		
		foreach (var renderer in Map.transform.GetComponentsInChildren<Renderer>())
		{
			renderer.enabled = false;
		}

		CameraManager.Instance.ChangeCamera(CameraManager.Instance.MenuSceneCamera);
		AudioManager.Instance.StopAllAudio();
		AudioManager.Instance.PlayMusic (AudioManager.Instance.MenuMusicAudioSource);

		IsMatchOver = true;
	}

    private bool IsPlayerInFirstTeam
    {
        get
        {
			return IsInFirstTeam(PhotonNetwork.player);
        }
    }

	static bool IsInFirstTeam(PhotonPlayer player)
	{
		return (bool)player.customProperties["IsInFirstTeam"];
	}

    private string LeftTeamName
    {
        get
        {
            return IsPlayerInFirstTeam ? "Alpha" : "Beta";
        }
    }

    private int LeftTeamScore
    {
        get
        {
            return IsPlayerInFirstTeam ? FirstTeamScore : SecondTeamScore;
        }
    }

    private string RightTeamName
    {
        get
        {
            return !IsPlayerInFirstTeam ? "Alpha" : "Beta";
        }
    }

    private int RightTeamScore
    {
        get
        {
            return !IsPlayerInFirstTeam ? FirstTeamScore : SecondTeamScore;
        }
    }

    private void OnFirstTeamScoredGoalEvent(byte eventCode, object content, int senderId)
    {
        if (eventCode != (byte)PhotonEventCodes.FirstTeamScoredGoal)
        {
            return;
        }

        ResetMatchAfterScoredGoal();

        var movementController = PlayerRootGameObject.transform.FindChild("MainPlayer").FindChild("Main Camera").GetComponent<MovementController>();
        var soundAudioSource = IsPlayerInFirstTeam ? movementController.ScoredGoalAudioSource : movementController.LostGoalAudioSource;
        AudioManager.Instance.PlaySound(soundAudioSource);

        ++FirstTeamScore;
        RefreshScoreLabels();
    }

    private void OnSecondTeamScoredGoalEvent(byte eventCode, object content, int senderId)
    {
        if (eventCode != (byte)PhotonEventCodes.SecondTeamScoredGoal)
        {
            return;
        }

        ResetMatchAfterScoredGoal();

        var movementController = PlayerRootGameObject.transform.FindChild("MainPlayer").FindChild("Main Camera").GetComponent<MovementController>();
        var soundAudioSource = IsPlayerInFirstTeam ? movementController.LostGoalAudioSource : movementController.ScoredGoalAudioSource;
        AudioManager.Instance.PlaySound(soundAudioSource);

        ++SecondTeamScore;
        RefreshScoreLabels();
    }

    private void RefreshScoreLabels()
    {
        GUIManager.Instance.InMatchScreen.FirstTeamScoreLabel.Text = String.Format("{0} : {1}", LeftTeamName, LeftTeamScore);
        GUIManager.Instance.InMatchScreen.SecondTeamScoreLabel.Text = String.Format("{1} : {0}", RightTeamName, RightTeamScore);
    }

	IEnumerator PlayerLeftGameMethod(PhotonPlayer player) {
	
		dfLabel label = GUIManager.Instance.InMatchScreen.PlayerLeftLabel;
		label.IsVisible = true;
		bool sameTeam = IsInFirstTeam (player) == IsInFirstTeam (PhotonNetwork.player); 
		label.Text = String.Format ("Player {0} [{1}] left the game", player.name, sameTeam ? "ally" : "enemy");

		yield return new WaitForSeconds(5);

		label.IsVisible = false;
	}

	public void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{

		StartCoroutine (PlayerLeftGameMethod(player));
	}

    private void OnGameEndedEvent(byte eventCode, object content, int senderId)
    {
        if (eventCode != (byte)PhotonEventCodes.GameEnded)
        {
            return;
        }

		EndMatch ();

        GUIManager.Instance.InMatchScreen.Hide();
        GUIManager.Instance.AfterMatchScreen.Initialize(String.Format("{0} Team", LeftTeamName), String.Format("{0} Team", RightTeamName), String.Format("{0} : {1}", LeftTeamScore, RightTeamScore));
        GUIManager.Instance.AfterMatchScreen.Show();
    }

    private void ResetMatchAfterScoredGoal()
    {
        ResetGameObjectPosition(PlayerRootGameObject, IsPlayerInFirstTeam ? FirstTeamRespawnTransform : SecondTeamRespawnTransform);
        if (PhotonNetwork.isMasterClient)
        {
            ResetGameObjectPosition(BallRootGameObject, BallRespawnTransform);
        }

        IsResettingAfterScoreNow = false;
    }

    private void ResetGameObjectPosition(GameObject obj, Transform spawnTransform)
    {
        obj.transform.position = spawnTransform.position;
        obj.transform.rotation = spawnTransform.rotation;

        obj.rigidbody.velocity = Vector3.zero;
        obj.rigidbody.angularVelocity = Vector3.zero;
    }

    public void Update()
    {
        if (!IsMatchOver)
        {
            float elapsedMatchTime = Time.time - BeginTime;
            double remainingMatchTime = MatchTimeInSeconds - elapsedMatchTime;
            TimeSpan remainingTimeSpan = remainingMatchTime > 0 ? TimeSpan.FromSeconds(remainingMatchTime) : TimeSpan.Zero;
            GUIManager.Instance.InMatchScreen.TimeLeftLabel.Text = String.Format("{0}:{1}", remainingTimeSpan.Minutes.ToString("D2"), remainingTimeSpan.Seconds.ToString("D2"));
            if (remainingMatchTime <= 0)
            {
                IsMatchOver = true;
                if (PhotonNetwork.isMasterClient)
                {
                    PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.GameEnded, null, true, new RaiseEventOptions() { Receivers = ExitGames.Client.Photon.Lite.ReceiverGroup.All });
                }
            }
        }
    }

    public GameObject FindPlayerRootObjById(int playerID)
    {
        var photonView = PlayersPhotonViews.FirstOrDefault(view => view.ownerId == playerID);
        return photonView != null ? photonView.gameObject : null;
    }
}
