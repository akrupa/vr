﻿using UnityEngine;
using System.Collections;

public class ApplicationManager : Singleton<ApplicationManager>
{
    protected ApplicationManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public void CloseApplication()
    {
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        #else
                Application.Quit();
        #endif
    }
}
