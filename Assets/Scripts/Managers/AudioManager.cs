﻿using UnityEngine;
using System.Collections;

public class AudioManager : Singleton<AudioManager>
{
    protected AudioManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public AudioSource MenuMusicAudioSource;
    public AudioSource MatchMusicAudioSource;

    public void PlaySound(AudioSource audioSource)
    {
        if (OptionsManager.Instance.IsSoundOn)
        {
            audioSource.Play();
        }
    }

    public void PlayMusic(AudioSource audioSource)
    {
        if (OptionsManager.Instance.IsMusicOn)
        {
            audioSource.Play();
        }
    }

    public void StopAllAudio()
    {
        foreach (AudioSource audioSource in GameObject.FindObjectsOfType<AudioSource>())
        {
            audioSource.Stop();
        }
    }
}
