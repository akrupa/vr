﻿using UnityEngine;
using System.Collections;

public class GUIManager : Singleton<GUIManager>
{
    protected GUIManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public MainMenuScreen MainMenuScreen;
    public OptionsScreen OptionsScreen;
    public InMatchScreen InMatchScreen;
    public AfterMatchScreen AfterMatchScreen;
    public MultiplayerMainScreen MultiplayerMainScreen;
    public MultiplayerCreateGameScreen MultiplayerCreateGameScreen;
    public MultiplayerWaitingScreen MultiplayerWaitingScreen;
	public EscapeScreen EscapeScreen;
	public GameObject[] maps;
}
