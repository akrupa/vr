﻿using UnityEngine;
using System.Collections;

public class CameraManager : Singleton<CameraManager>
{
    protected CameraManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public Camera CurrentCamera;
    public Camera MenuSceneCamera;
    public Camera PlayerCamera;
    public SpaceCameraTargetMovementController spaceCameraTarget;

    public void ChangeCamera(Camera newCamera)
    {
        if (CurrentCamera == MenuSceneCamera)
        {
            spaceCameraTarget.gameObject.SetActive(false);
        }
        if (CurrentCamera != null)
        {
            CurrentCamera.gameObject.SetActive(false);
        }
        CurrentCamera = newCamera;
        CurrentCamera.gameObject.SetActive(true);
        if (CurrentCamera == MenuSceneCamera)
        {
            spaceCameraTarget.gameObject.SetActive(true);
        }
    }
}
