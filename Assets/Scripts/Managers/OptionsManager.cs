﻿using UnityEngine;
using System.Collections;

public class OptionsManager : Singleton<OptionsManager>
{
    protected OptionsManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public string PlayerNickname = "NoName";
    public bool IsSoundOn = true;
    public bool IsMusicOn = true;

	private const string musicPrefsKey = "MusicKey";
	private const string soundsPrefsKey = "SoundsKey";
	private const string nicknamePrefsKey = "NicknameKey";

	private bool isMusicPlaying;

	private void Start() {
		if (PlayerPrefs.HasKey(musicPrefsKey)) {
			IsMusicOn = PlayerPrefs.GetInt (musicPrefsKey) == 1;
		}
		if (PlayerPrefs.HasKey(soundsPrefsKey)) {
			IsSoundOn = PlayerPrefs.GetInt (soundsPrefsKey) == 1;
		}
		if (PlayerPrefs.HasKey(nicknamePrefsKey)) {
			PlayerNickname = PlayerPrefs.GetString (nicknamePrefsKey);
		}

		isMusicPlaying = IsMusicOn;
		if (isMusicPlaying)
		{
			AudioManager.Instance.PlayMusic(AudioManager.Instance.MenuMusicAudioSource);
		}
		else
		{
			AudioManager.Instance.StopAllAudio();
		}
	}

	public void Save() {
		PlayerPrefs.SetInt (musicPrefsKey, IsMusicOn ? 1 : 0);
		PlayerPrefs.SetInt (soundsPrefsKey, IsSoundOn ? 1 : 0);
		PlayerPrefs.SetString (nicknamePrefsKey, PlayerNickname);
		PlayerPrefs.Save ();

		if (IsMusicOn != isMusicPlaying)
		{
			isMusicPlaying = IsMusicOn;
			if (isMusicPlaying)
			{
				AudioManager.Instance.PlayMusic(AudioManager.Instance.MenuMusicAudioSource);
			}
			else
			{
				AudioManager.Instance.StopAllAudio();
			}
		}
	}
}