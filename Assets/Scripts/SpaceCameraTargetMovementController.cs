﻿using UnityEngine;
using System.Collections;

public class SpaceCameraTargetMovementController : MonoBehaviour
{
    public float ScalingFactor = 1.0f;

    void FixedUpdate()
    {
        transform.Rotate(Vector3.up, Time.fixedDeltaTime * ScalingFactor);
    }
}
